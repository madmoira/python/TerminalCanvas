from blessed import Terminal

from commands import execute_command
from draw_canvas import DrawCanvas
from exceptions import CommandException


term = Terminal()

with term.fullscreen(), term.hidden_cursor():
    with term.cbreak():
        val = ''
        command = ''
        current_canvas = DrawCanvas(0, 0)
        print(term.move(term.height, 0) + 'Enter command: {}'.format(command))
        while val.lower() not in ['q', 'Q']:
            val = term.inkey()
            print(term.clear)

            if val.name == 'KEY_ENTER':
                if command == '':
                    print(term.move(term.height, 0) + 'Invalid command')
                    command = ''
                    continue
                try:
                    executed = execute_command(command, current_canvas)
                except CommandException:
                    print(term.move(term.height, 0) + 'Invalid command')
                    command = ''
                    continue
                if not executed:
                    print(term.move(term.height, 0) + 'Invalid command')
                command = ''
            elif val.name == 'KEY_DELETE':
                if command:
                    command = command[:-1]
            else:
                if not val.is_sequence:
                    command += str(val)
                    
            print(term.move(term.height, 0) + 'Enter command: {}'.format(command))
            current_canvas.draw(term)
