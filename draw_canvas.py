from exceptions import CommandException


class DrawCanvas(object):
    def __init__(self, height, width):
        self.initialize_canvas(height, width)

    def initialize_canvas(self, height, width):
        self.height = height
        self.width = width
        self.structure = [[' ' for x in range(self.width + 2)]
                          for y in range(self.height + 2)]
        self.fill_borders()

    def fill_borders(self):
        for row_index, row in enumerate(self.structure):
            for col_index, _ in enumerate(row):
                if row_index == 0 or row_index == self.height + 1:
                    self.structure[row_index][col_index] = '-'
                elif col_index == 0 or col_index == self.width + 1:
                    self.structure[row_index][col_index] = '|'

    def to_draw(self):
        return [''.join(row) for row in self.structure]

    def draw_line(self, point_a, point_b):
        if point_a == point_b:
            raise CommandException

        if point_a['x'] == point_b['x']:
            current_y = min([point_a['y'], point_b['y']])
            while current_y <= max([point_a['y'], point_b['y']]):
                self.structure[current_y][point_a['x']] = 'X'
                current_y += 1

        elif point_a['y'] == point_b['y']:
            current_x = min([point_a['x'], point_b['x']])
            while current_x <= max([point_a['x'], point_b['x']]):
                self.structure[point_a['y']][current_x] = 'X'
                current_x += 1
        
        else:
            raise CommandException

    def get_element(self, point):
        return self.structure[point['y']][point['x']]

    def draw_rectangle(self, point_a, point_b):
        point_c = {'x': point_a['x'], 'y': point_b['y']}
        point_d = {'x': point_b['x'], 'y': point_a['y']}
        self.draw_line(point_a, point_c)
        self.draw_line(point_c, point_b)
        self.draw_line(point_b, point_d)
        self.draw_line(point_d, point_a)

    def fill_area(self, point_a, color):
        if self.structure[point_a['y']][point_a['x']] in ['-', '|', 'X']:
            raise CommandException
        
        wall_list = ['-', '|', 'X', color]
        point_list = [point_a]
        visited_points = []

        while len(point_list) > 0:
            current_point = point_list.pop()
            visited_points.append(current_point)

            self.structure[current_point['y']][current_point['x']] = color

            up_point = {'x': current_point['x'],'y': current_point['y'] - 1}
            down_point = {'x': current_point['x'],'y': current_point['y'] + 1}
            left_point = {'x': current_point['x'] - 1,'y': current_point['y']}
            right_point = {'x': current_point['x'] + 1,'y': current_point['y']}
            
            if up_point not in visited_points and self.get_element(up_point) not in wall_list:
                point_list.append(up_point)
            
            if down_point not in visited_points and self.get_element(down_point) not in wall_list:
                point_list.append(down_point)

            if left_point not in visited_points and self.get_element(left_point) not in wall_list:
                point_list.append(left_point)

            if right_point not in visited_points and self.get_element(right_point) not in wall_list:
                point_list.append(right_point)

    def draw(self, terminal):
        if self.height == 0 or self.width == 0:
            return

        with terminal.location(x=0, y=0):
            for row in self.to_draw():
                print(row)
