# Terminal drawings

## Getting Started

The application requires Blessed, for that reason, this small app only works for UNIX terminals (Linux / macOS).

Create a virtual environment, activate it and install the requirements.

```
pip install -r requirements.txt
```

Execute the application

```
python main.py
```

## Example
Execute the following sequence of commands

```
C 5 5
L 3 1 5 1
R 1 2 3 5
B 5 2 i
```

You will obtain the following drawing:

```
-------
|  XXX|
|XXXii|
|X Xii|
|X Xii|
|XXXii|
-------
```

## Reference

Create a new Canvas:

```
C width height
```

Draw a line:

```
L x1 y1 x2 y2
```

Draw a rectangle:

```
R x1 y1 x2 y2
```

Fill with color:

```
B x1 y1 color
```

## Tests

To run the test suite, run this command:

```
pytest
```
