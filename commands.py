from exceptions import CommandException

class BaseCommand(object):
    command_letter = ''

    def __init__(self, command):
        self.command = command
        self.params = self.command.split(' ')[1:]

    def validate_params(self):
        try:
            self.params = [int(param) for param in self.params]
        except ValueError:
            raise CommandException

    def execute(self):
        raise NotImplementedError


class CreateCanvas(BaseCommand):
    command_letter = 'C'

    def validate_params(self):
        if len(self.params) != 2:
            raise CommandException
        super(CreateCanvas, self).validate_params()

    def execute(self, canvas):
        return canvas.initialize_canvas(self.params[1], self.params[0])


class DrawLine(BaseCommand):
    command_letter = 'L'

    def validate_params(self):
        if len(self.params) != 4:
            raise CommandException
        super(DrawLine, self).validate_params()

    def execute(self, canvas):
        point_a = {'x': self.params[0], 'y': self.params[1]}
        point_b = {'x': self.params[2], 'y': self.params[3]}
        canvas.draw_line(point_a, point_b)


class DrawRectangle(BaseCommand):
    command_letter = 'R'

    def validate_params(self):
        if len(self.params) != 4:
            raise CommandException
        super(DrawRectangle, self).validate_params()

    def execute(self, canvas):
        point_a = {'x': self.params[0], 'y': self.params[1]}
        point_b = {'x': self.params[2], 'y': self.params[3]}
        canvas.draw_rectangle(point_a, point_b)


class FillColor(BaseCommand):
    command_letter = 'B'

    def validate_params(self):
        if len(self.params) != 3:
            raise CommandException
        
        try:
            self.params[0] = int(self.params[0])
            self.params[1] = int(self.params[1])
        except ValueError:
            raise CommandException

    def execute(self, canvas):
        point_a = {'x': self.params[0], 'y': self.params[1]}
        canvas.fill_area(point_a, self.params[2])


available_commands = [
    CreateCanvas,
    DrawLine,
    DrawRectangle,
    FillColor,
]

def execute_command(command_input, canvas):

    if command_input[0] != 'C' and (canvas.height == 0 or canvas.width == 0):
        raise CommandException

    executed = False
    for command in available_commands:
        if command.command_letter == command_input[0]:
            executed = True
            cmd_instance = command(command_input)
            cmd_instance.validate_params()
            cmd_instance.execute(canvas)
            break
    return executed
    
