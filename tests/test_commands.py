import pytest

from commands import execute_command
from draw_canvas import DrawCanvas
from exceptions import CommandException


class TestCommands(object):

    def test_invalid_no_valid_canvas(self):
        cv = DrawCanvas(0, 0)
        with pytest.raises(CommandException):
            execute_command('L 1 1 2 1', cv)

    def test_create_canvas_command(self):
        cv = DrawCanvas(0, 0)
        executed = execute_command('C 3 3', cv)
        rows = cv.to_draw()

        expected_result = [
            '-----',
            '|   |',
            '|   |',
            '|   |',
            '-----',
        ]

        assert executed == True
        assert rows == expected_result

    def test_invalid_create_canvas(self):
        cv = DrawCanvas(0, 0)
        with pytest.raises(CommandException):
            execute_command('C a a', cv)
        with pytest.raises(CommandException):
            execute_command('C 1 2 3', cv)

    def test_line_command(self):
        cv = DrawCanvas(3, 3)
        executed = execute_command('L 1 1 3 1', cv)
        rows = cv.to_draw()

        expected_result = [
            '-----',
            '|XXX|',
            '|   |',
            '|   |',
            '-----',
        ]

        assert executed == True
        assert rows == expected_result

    def test_invalid_line_command(self):
        cv = DrawCanvas(4, 4)
        with pytest.raises(CommandException):
            execute_command('L 1 a 2 b', cv)
        with pytest.raises(CommandException):
            execute_command('L 1 1 1 1 1', cv)
        with pytest.raises(CommandException):
            execute_command('L 2 2 2 2', cv)

    def test_rectangle_command(self):
        cv = DrawCanvas(4, 4)
        executed = execute_command('R 1 1 4 3', cv)
        rows = cv.to_draw()

        expected_result = [
            '------',
            '|XXXX|',
            '|X  X|',
            '|XXXX|',
            '|    |',
            '------',
        ]

        assert executed == True
        assert rows == expected_result

    def test_invalid_rectangle_command(self):
        cv = DrawCanvas(4, 4)
        with pytest.raises(CommandException):
            execute_command('R 2 2 2 2', cv)
        with pytest.raises(CommandException):
            execute_command('R a 2 2 2', cv)
        with pytest.raises(CommandException):
            execute_command('R 2 2 2 2 2', cv)

    def test_fill_command(self):
        cv = DrawCanvas(3, 3)
        executed = execute_command('B 1 1 o', cv)
        rows = cv.to_draw()

        expected_result = [
            '-----',
            '|ooo|',
            '|ooo|',
            '|ooo|',
            '-----',
        ]

        assert executed == True
        assert rows == expected_result

    def test_invalid_fill(self):
        cv = DrawCanvas(3, 3)
        execute_command('L 1 1 3 1', cv)
        with pytest.raises(CommandException):
            execute_command('B 2 1 o', cv)
        with pytest.raises(CommandException):
            execute_command('B 2 1', cv)
        with pytest.raises(CommandException):
            execute_command('B a 1 o', cv)

    def test_command_sequence(self):
        cv = DrawCanvas(0, 0)
        execute_command('C 5 5', cv)
        execute_command('L 1 1 3 1', cv)
        execute_command('L 1 1 1 4', cv)
        execute_command('R 2 2 4 4', cv)
        execute_command('B 4 1 o', cv)
        rows = cv.to_draw()

        expected_result = [
            '-------',
            '|XXXoo|',
            '|XXXXo|',
            '|XX Xo|',
            '|XXXXo|',
            '|ooooo|',
            '-------',
        ]

        assert rows == expected_result

    def test_invalid_command(self):
        cv = DrawCanvas(1, 1)
        executed = execute_command('a a a a', cv)
        assert executed == False
