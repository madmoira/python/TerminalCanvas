import pytest

from draw_canvas import DrawCanvas

class TestCanvas(object):

    def test_create_canvas(self):
        cv = DrawCanvas(1, 1)
        rows = cv.to_draw()
        assert len(rows) == 3
        assert len(rows[0]) == 3

        expected_result = [
            '---',
            '| |',
            '---',
        ]

        assert rows == expected_result

    def test_create_horizontal_line(self):
        cv = DrawCanvas(3, 3)
        point_a = {'x': 1, 'y': 1}
        point_b = {'x': 3, 'y': 1}
        cv.draw_line(point_a, point_b)
        rows = cv.to_draw()

        expected_result = [
            '-----',
            '|XXX|',
            '|   |',
            '|   |',
            '-----',
        ]

        assert rows == expected_result

    def test_create_vertical_line(self):
        cv = DrawCanvas(3, 3)
        point_a = {'x': 1, 'y': 1}
        point_b = {'x': 1, 'y': 3}
        cv.draw_line(point_a, point_b)
        rows = cv.to_draw()

        expected_result = [
            '-----',
            '|X  |',
            '|X  |',
            '|X  |',
            '-----',
        ]

        assert rows == expected_result

    def test_create_rectangle(self):
        cv = DrawCanvas(4, 4)
        point_a = {'x': 1, 'y': 1}
        point_b = {'x': 3, 'y': 3}
        cv.draw_rectangle(point_a, point_b)
        rows = cv.to_draw()

        expected_result = [
            '------',
            '|XXX |',
            '|X X |',
            '|XXX |',
            '|    |',
            '------',
        ]

        assert rows == expected_result

    def test_fill_all_canvas(self):
        cv = DrawCanvas(3, 3)
        point_a = {'x': 1, 'y': 1}
        cv.fill_area(point_a, 'o')
        rows = cv.to_draw()

        expected_result = [
            '-----',
            '|ooo|',
            '|ooo|',
            '|ooo|',
            '-----',
        ]

        assert rows == expected_result

    def test_fill_rectangle(self):
        cv = DrawCanvas(4, 4)
        point_a = {'x': 1, 'y': 1}
        point_b = {'x': 3, 'y': 3}
        cv.draw_rectangle(point_a, point_b)
        point_c = {'x': 2, 'y': 2}
        cv.fill_area(point_c, 'o')
        rows = cv.to_draw()

        expected_result = [
            '------',
            '|XXX |',
            '|XoX |',
            '|XXX |',
            '|    |',
            '------',
        ]

        assert rows == expected_result

    def test_fill_outside_rectangle(self):
        cv = DrawCanvas(4, 4)
        point_a = {'x': 1, 'y': 1}
        point_b = {'x': 3, 'y': 3}
        cv.draw_rectangle(point_a, point_b)
        point_c = {'x': 4, 'y': 1}
        cv.fill_area(point_c, 'o')
        rows = cv.to_draw()

        expected_result = [
            '------',
            '|XXXo|',
            '|X Xo|',
            '|XXXo|',
            '|oooo|',
            '------',
        ]

        assert rows == expected_result

    def test_fill_corner(self):
        cv = DrawCanvas(4, 4)
        point_a = {'x': 1, 'y': 2}
        point_b = {'x': 2, 'y': 2}
        cv.draw_line(point_a, point_b)
        point_c = {'x': 3, 'y': 2}
        point_d = {'x': 3, 'y': 4}
        cv.draw_line(point_c, point_d)
        point_e = {'x': 1, 'y': 4}
        cv.fill_area(point_e, 'o')
        rows = cv.to_draw()

        expected_result = [
            '------',
            '|    |',
            '|XXX |',
            '|ooX |',
            '|ooX |',
            '------',
        ]

        assert rows == expected_result
